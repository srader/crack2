//OPTIMIZATION DONE:
//-O2 flag in makefile

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"



const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
const int STEP = 100;


// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    
//    guess[strlen(guess) - 1] = '\0';
    guess = md5(guess, strlen(guess) );


    // Compare the two hashes
//    printf("Trying %s and %s\n", hash, guess);
    
    if(strcmp(hash, guess) == 0)
    {return 1;}
    else
    {return 0;}
    
    // Free any malloc'd memory
    //change code
}

char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        fprintf(stderr, "Can't open %s for reading\n", filename);
        return NULL;
    }
    
    int arrlen = 0;
    
    //allocate space for arrlen char *
    char **lines = NULL;
    
    char buf[1000];
    int i = 0;
    while (fgets(buf, 1000, f))
    {
        //check if array is full. If so, extend it.
        if(i == arrlen)
        {
            arrlen += STEP;
            
            char ** newline = (char **)realloc(lines, arrlen * sizeof(char *));
            if (!newline)
            {
                fprintf(stderr, "Can't realloc\n");
                exit(1);
            }
            lines = newline;
            
        }
        // trim off newline char
        buf[strlen(buf) - 1] = '\0';
        
        //get length of buf
        int slen = strlen(buf);
        
        //allocate space for the string
        char *str = (char *)malloc((slen+1) * sizeof(char));
        
        //copy string from buf to str
        strcpy(str, buf);
        
        //attach str to data structure
        lines[i] = str;
        
        i++;
    }
    
    
    if(i == arrlen)
    {
        char **newarr = (char **)realloc(lines, (arrlen+1)*sizeof(char *));
        if (!newarr)
        {
            fprintf(stderr, "can't realloc\n");
            exit(1);
        }
        
    }
    
    lines[i] = NULL;
    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }





    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    // one loop for hashes, one for dict

    
    for(int h = 0; hashes[h] != NULL; h++)
    {

        for(int d = 0; dict[d] != NULL; d++)
        {

            if(tryguess(hashes[h], dict[d]) == 1)
            {
                printf("Found! %s :: %s\n", hashes[h], dict[d]);
            }

           
        }
    }


    free(hashes);
    free(dict);
    printf("Done\n");
    

    
}
